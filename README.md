GitLab Repo : https://gitlab.com/Narender-Gusain/nagp-docker-project.git  
Docker Hub Url : https://hub.docker.com/r/narendergusain26/node-project-image  
Docker Image Url : narendergusain26/node-project-image:tagname  
Latest Docker image : narendergusain26/node-project-image:v5.15  
Video Link :  https://nagarro-my.sharepoint.com/:v:/p/narender_gusain/EQG0wpgeVi1CiBCONER9OrEBFloefEWTi3EBIzH_sauZ0A  
  
Services Url's : /insert   : This service url is used to insert value into database.  
                 /records  : This service url is used to get all records in database.  
Example : http://4.172.11.66/records  
Example : http://4.172.11.66/insert  

Instruction related to annotation :  

In "_apiservice.yaml" file, i used below annotation as it was required by the Azure kubernetes service.  
If you are also using azure kubernetes service, you can use below annotation but just change your resource group name with the value "kube1" as it was my resource group name.  
For GCP user, you can remove this line.  

 annotations:  
    service.beta.kubernetes.io/azure-load-balancer-resource-group: kube1  


