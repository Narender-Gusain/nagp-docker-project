const express = require('express');
const mysql = require('mysql2');
const app = express();
const port = 3000;

const db = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME
});

db.connect((err) => {
  if (err) {
    console.error('Database connection failed:', err.stack);
    return;
  }
  console.log('Connected to database.');
});


app.get('/insert', (req, res) => {
  db.query(`INSERT INTO employee (name, email) VALUES ('Narender Gusain', 'abc@example.com')`,
   (error, results) => {
    if (error) {
      res.status(500).send(error.toString());
    } else {
      console.log(`Show response : ${res}`);
      // res.json(results);
      res.send(JSON.stringify(results));
    }
  });
});

app.get('/records', (req, res) => {
  db.query('SELECT * FROM employee', (error, results) => {
    if (error) {
      res.status(500).send(error.toString());
      console.log(`Records error response : ${res}`);
    } else {
      // res.json(results);
      console.log(`Show response : ${res}`);
      res.send(JSON.stringify(results));
    }
  });
});

app.get('/**', (req, res) => {
  db.query(` CREATE TABLE IF NOT EXISTS employee (
              id INT AUTO_INCREMENT PRIMARY KEY,
              name VARCHAR(255) NOT NULL,
              email VARCHAR(255) NOT NULL)`, (err, result) => {
    if (err) {
      console.error('Error creating table:', err);
      return;
    }
    console.log('Table created successfully');
  })
  res.send("Welcome Narender");
});

app.listen(port, () => {
  console.log(`API server running on port ${port}`);
});
