FROM node:22-alpine3.18

ENV PORT=3300

WORKDIR /app

COPY package*.json ./
RUN npm install

COPY . .
EXPOSE 3300
CMD [ "npm","start" ]


